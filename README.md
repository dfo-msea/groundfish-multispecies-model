# Disentangling the impacts of environmental change and commercial fishing on groundfish biodiversity in a northeast Pacific ecosystem.

Patrick L. Thompson - patrick.thompson@dfo-mpo.gc.ca  
Sean C. Anderson, Jessica Nephin, Dana R. Haggarty, M. Angelica Peña, Philina A. English, Katie S. P. Gale, Emily Rubidge

### Abstract 

Conservation of marine biodiversity requires understanding the joint influence of ongoing environmental change and fishing pressure. Addressing this challenge requires robust biodiversity monitoring and analyses that jointly account for potential drivers of change. Here, we ask how groundfish biodiversity in Canadian Pacific waters has changed since 2003 and assess the degree to which these changes can be explained by environmental change and commercial fishing. Using a spatiotemporal multispecies model based on fisheries independent data, we find that species richness and community biomass have increased during this period. Environmental changes during this period were associated with temporal fluctuations in the biomass of species and the community as a whole. However, environmental change was less associated with changes in species occurrence and is not likely to be responsible for the estimated increases in biodiversity. Instead, our results are consistent with an ongoing recovery of the groundfish community from a reduction in commercial fishing intensity from historical levels. These findings provide key insight into the drivers of biodiversity change that can inform ecosystem based management.
