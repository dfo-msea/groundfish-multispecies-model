# Processing trawl data

*  Suggest removing some of the longer tows. Tows travelling over greater distances than expected in 20 minute tows are likely lat/long errors (similar to the change in the depth cut-off). You could ask Maria what a reasonable cut-off distance might be or calculate based on the max towing speed.
*  We didn't discuss this cut-off: "Only include species that are present in at least 50 trawls across all years in the dataset." Wouldn't this remove all rare species? Is this necessary to reduce zeros in the dataset or just to have a more manageable dataset to work with? I'm wondering what percentage of species that equates to and how this might affect the results.
*  Method for linking environmental raster to trawls: I didn't find extracting the values overlapping with the lines to be too slow a process. Since this is something you don't have to repeat often it may be worth using the lines themselves. If you find that it is just too slow then the 100m interval is probably the best approach and would achieve similar results to the line method given the resolution of the raster.


# Environmental data

*  BPIs and slope (the derivatives of depth) are all calculated from the 100m bathymetry model (so 100m native resolution). You have MBPI at 20m which is incorrect (that was just for the nearshore models in our paper).
*  Chl-a, I have code to derive this layer from MODIS satellite outputs (but is time consuming, as it requires computationally heavy raster processing). Not sure this will be worthwhile, all other variables are modelled bottom values while Chl-a is a surface level proxy, so likely the most distal predictor.
*  We could connect with Angelina Pena (who is now leading the BC ROMS model development) or others using the more recent BC ROMS outputs (Chris Rooper) to get the more recent data and then calculate yearly (or bi-yearly) layers. I have code for working with their netcdf outputs. They may have nutrient variables as well now. The resolution is still 3km.
*  I think it makes sense to keep the static bottom current speed layers. These would not vary much from year to year.


# CV

*  I'm trying to wrap my head around the spatial and temporal random factors and how they work with the CV approach. For example, if you were simply using 'year' to create folds, when you fit a model with training data from 2002 to 2015 and evaluate it using 2016/2017 data, are the temporal random fields derived from data that include the holdout 2016/2017 years or is that holdout data completely independent from the temporal random fields? If the random factors are derived from the entire dataset, than the block CV approach won't work as expected.   
*  I agree that is would be good idea to do spatial and temporally blocking together. Maybe using less spatial folds would be required, so you end up with not too many folds overall? Perhaps 3 temporal bins and 3 space bins, so 9 total. Below are some links to CV papers and that R function that creates 'spacetimefolds'. I haven't used it myself, just came across it when working on the SDM paper.

## good CV blocking paper
https://onlinelibrary.wiley.com/doi/10.1111/ecog.02881

## Space/time fold creation
*  https://cran.r-project.org/web/packages/CAST/vignettes/CAST-intro.html
*  https://rdrr.io/cran/CAST/man/CreateSpacetimeFolds.html
*  Paper that the method is based on: https://www.sciencedirect.com/science/article/pii/S1364815217310976?via%3Dihub
