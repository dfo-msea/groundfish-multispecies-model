library(tidyverse)
library(Hmsc)
library(rstan)

#evaluate covergence###
load("./models/pa_tmp_models_thin_1000_samples_250_chains_4.Rdata")
load("./models/abnd_tmp_models_thin_100_samples_250_chains_4.Rdata")
load("./models/depth_mod/depth_pa_tmp_models_thin_1000_samples_250_chains_4.Rdata")
load("./models/depth_mod/depth_abnd_tmp_models_thin_100_samples_250_chains_4.Rdata")


mod_list <- list(m_pa, m_abnd)
mod_list_depth = list(m_pa_depth, m_abnd_depth)
mod_names <- factor(c("Presence absence", "Conditional biomass", "Depth_pa", "Depth_abund"), levels = c("Presence absence", "Depth_pa", "Conditional biomass", "Depth_abund"), ordered = TRUE)
mod_names_depth = mod_names[c(3:4)]
mod_names <- mod_names[c(1:2)]
nm <- length(mod_list)

save(mod_list, mod_names, nm, file = "./models/selected_models.RData")
save(mod_list_depth, mod_names_depth, file = "./models/selected_models_depth.RData")

mcmc.list2array <-
  function(x) {
    arr <- array(NA_real_,
                 dim = c(nrow(x[[1]]), length(x), ncol(x[[1]])))
    dimnames(arr)[[3]] <- colnames(x[[1]])
    for(i in seq_along(x)) arr[,i,] <- as.matrix(x[[i]])
    return(arr)
  }

converge.df <- data.frame()
for(i in 1:nm){
  m <- mod_list[[i]]
  mpost <- convertToCodaObject(m)
  beta <- mcmc.list2array(mpost$Beta)
  ge.beta <- sapply(1:dim(beta)[3], FUN = function(x) Rhat(sims = beta[,,x]))
  gamma <- mcmc.list2array(mpost$Gamma)
  ge.gamma <- sapply(1:dim(gamma)[3], FUN = function(x) Rhat(sims = gamma[,,x]))
  V <- mcmc.list2array(mpost$V)
  ge.V <- sapply(1:dim(V)[3], FUN = function(x) Rhat(sims = V[,,x]))
  Omega_trawl <- mcmc.list2array(mpost$Omega[[1]])
  ge.omega_trawl <- sapply(1:dim(Omega_trawl)[3], FUN = function(x) Rhat(sims = Omega_trawl[,,x]))
  Omega_time <- mcmc.list2array(mpost$Omega[[2]])
  ge.omega_time <- sapply(1:dim(Omega_time)[3], FUN = function(x) Rhat(sims = Omega_time[,,x]))

  converg_1 <- rbind(data.frame(parameter = "\u03b2", ge = ge.beta),
        data.frame(parameter = "\u03b3", ge = ge.gamma),
        data.frame(parameter = "V", ge = ge.V)
        #data.frame(parameter = "\u03A9 trawl", ge = ge.omega_trawl),
        #data.frame(parameter = "\u03A9 time",  ge = ge.omega_time)
        )
  converg_1$model <- mod_names[i]

  converge.df <- rbind(converge.df,converg_1)
}

data.frame(Species = rep(colnames(mod_list[[1]]$Y), each = ncol(m$XScaled)), param = colnames(m$XScaled), ge.beta)[which(ge.beta>1.05),]

ggplot(converge.df, aes(x = parameter, y = ge, color = model, group = interaction(parameter, model)))+
  geom_hline(yintercept = c(1.05, 1.1), linetype = 2)+
  geom_boxplot()+
  scale_color_manual(values = c(1,2,3,4))
ggsave("./figures/convergence/gelman.png", height = 6, width = 8)

#plot(mpost$Beta)
save(converge.df, file = "./data/convergence.RData")

